#!/bin/bash

set -e

# allow custom tag
[ -z $DOCKER_TAG ] && export DOCKER_TAG=${CI_BUILD_TAG}

# set vars in project settings
VARS=( IMAGE_NAME REGISTRY NAMESPACE DOCKER_TAG )

. <(curl -fsSL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/check-vars.sh)

check_vars

# build
docker_build() {
  docker build --pull -t ${IMAGE_NAME,,}:${DOCKER_TAG} .
}

# tag
docker_tag() {
  docker tag ${IMAGE_NAME,,}:${DOCKER_TAG} ${REGISTRY}/${NAMESPACE,,}/${IMAGE_NAME,,}:${DOCKER_TAG}
}

# push
docker_push() {
  docker push ${REGISTRY}/${NAMESPACE,,}/${IMAGE_NAME,,}:${DOCKER_TAG}
  if [ -z $KEEP_LOCAL_IMAGES ]; then
    echo "cleanup: removing local docker image (set \$KEEP_LOCAL_IMAGES in project settings to some value to keep local builds)"
    local IMAGE_ID=$(docker images -q  ${REGISTRY}/${NAMESPACE,,}/${IMAGE_NAME,,}:${DOCKER_TAG})
    docker rmi ${REGISTRY}/${NAMESPACE,,}/${IMAGE_NAME,,}:${DOCKER_TAG}
    docker rmi ${IMAGE_ID}
  fi 
}
